<?php
/*
 */
namespace Chill\ReportBundle\Export\Export;

use Doctrine\ORM\EntityManagerInterface;
use Chill\CustomFieldsBundle\Entity\CustomFieldsGroup;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\ReportBundle\Entity\Report;
use Chill\MainBundle\Export\ExportElementsProviderInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Chill\CustomFieldsBundle\Service\CustomFieldProvider;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ReportListProvider implements ExportElementsProviderInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    /**
     *
     * @var CustomFieldProvider
     */
    protected $customFieldProvider;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    function __construct(
        EntityManagerInterface $em, 
        TranslatableStringHelper $translatableStringHelper,
        TranslatorInterface $translator,
        CustomFieldProvider $customFieldProvider
    ) {
        $this->em = $em;
        $this->translatableStringHelper = $translatableStringHelper;
        $this->translator = $translator;
        $this->customFieldProvider = $customFieldProvider;
    }

    
    
    public function getExportElements()
    {
        $groups = $this->em->getRepository(CustomFieldsGroup::class)
            ->findBy([ 'entity' => Report::class ])
            ;
        $reports = [];
        
        foreach ($groups as $group) {
            $reports[$group->getId()] = new ReportList(
                $group, 
                $this->translatableStringHelper,
                $this->translator,
                $this->customFieldProvider,
                $this->em);
        }
        
        return $reports;
    }
}
