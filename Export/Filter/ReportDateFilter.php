<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Chill\ReportBundle\Export\Filter;

use Chill\MainBundle\Export\FilterInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Doctrine\ORM\Query\Expr;
use Chill\MainBundle\Form\Type\ChillDateType;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ReportDateFilter implements FilterInterface
{

    public function addRole()
    {
        return null;
    }

    public function alterQuery(\Doctrine\ORM\QueryBuilder $qb, $data)
    {
        $where = $qb->getDQLPart('where');
        $clause = $qb->expr()->between('report.date', ':report_date_filter_date_from', 
            ':report_date_filter_date_to');

        if ($where instanceof Expr\Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }
        
        $qb->add('where', $where);
        $qb->setParameter('report_date_filter_date_from', $data['date_from']);
        $qb->setParameter('report_date_filter_date_to', $data['date_to']);
    }

    public function applyOn()
    {
        return 'report';
    }

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder)
    {
        $builder->add('date_from', ChillDateType::class, array(
            'label' => "Report is after this date",
            'data'  => new \DateTime(),
        ));
        
        $builder->add('date_to', ChillDateType::class, array(
            'label' => "Report is before this date",
            'data'  => new \DateTime(),
        ));
    }

    public function describeAction($data, $format = 'string')
    {
        return array('Filtered by report\'s date: '
            . 'between %date_from% and %date_to%', array(
                '%date_from%' => $data['date_from']->format('d-m-Y'),
                '%date_to%'   => $data['date_to']->format('d-m-Y')
            ));
    }

    public function getTitle()
    {
        return 'Filter by report\'s date';
    }
}
