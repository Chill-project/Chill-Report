<?php

/*
 * Copyright (C) 2015 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ReportBundle\Security\Authorization;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\ReportBundle\Entity\Report;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Entity\Center;


/**
 *
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ReportVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    const CREATE = 'CHILL_REPORT_CREATE';
    const SEE    = 'CHILL_REPORT_SEE';
    const UPDATE = 'CHILL_REPORT_UPDATE';
    const LISTS  = 'CHILL_REPORT_LISTS';

    /**
     *
     * @var AuthorizationHelper
     */
    protected $helper;


    public function __construct(AuthorizationHelper $helper)
    {
        $this->helper = $helper;
    }


    protected function supports($attribute, $subject)
    {
        if ($subject instanceof Report) {
            return \in_array($attribute, [
                self::CREATE, self::UPDATE, self::SEE
            ]);
        } elseif ($subject instanceof Center) {
            return $attribute === self::LISTS;
        }
    }


    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if (!$token->getUser() instanceof User) {
            return false;
        }
        return $this->helper->userHasAccess($token->getUser(), $subject, $attribute);
    }


    public function getRoles()
    {
        return [self::CREATE, self::UPDATE, self::SEE, self::LISTS];
    }

    public function getRolesWithoutScope()
    {
        return array(self::LISTS);
    }

    public function getRolesWithHierarchy()
    {
        return [ 'Report' => $this->getRoles() ];
    }
}
